package cl.nisum.api.registro.services;


import cl.nisum.api.registro.models.Phone;
import cl.nisum.api.registro.models.User;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceTest {

    @Autowired
    private UserService userService;

    @Test
    public void whenApplicationStarts() {
        List<User> books = userService.list();

        Assert.assertEquals(books.size(), 0);
    }

    @Test
    public void whenCreacionUsuario() throws Exception {
        User user = new User();
        user.setActive(true);
        user.setCreated(new Date());
        user.setEmail("riverosluco@gmail.com");
        user.setLastLogin(new Date());
        user.setName("Carlos Riveros Luco");
        Phone phone = new Phone();
        phone.setCityCode("1");
        phone.setContryCode("2");
        phone.setNumber("11212112");
        phone.setUser(user);
        Set<Phone> phones = new HashSet<>();
        phones.add(phone);
        user.setPhones(phones);
        userService.create(user);
        Assert.assertEquals(userService.list().size(), 1);

    }
}
