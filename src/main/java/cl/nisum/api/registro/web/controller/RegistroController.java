package cl.nisum.api.registro.web.controller;

import cl.nisum.api.registro.models.User;
import cl.nisum.api.registro.services.UserService;
import cl.nisum.api.registro.services.exception.RegistroException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/registro/v1/")
public class RegistroController {

    @Autowired
    transient UserService userService;


    @PostMapping(path = "/", consumes = "application/json", produces = "application/json")
    public User creacion(@RequestBody User user) {

        return userService.create(user);
    }

    @GetMapping("/{id}")
    public User getUsuario(@PathVariable Long id) {
        User user = userService.get(id);
        if (user == null)
            throw new RegistroException("Id: " + id + " no encontrado");
        return userService.get(id);
    }

    @GetMapping
    public List<User> getUsuarios() {
        return userService.list();
    }

}
