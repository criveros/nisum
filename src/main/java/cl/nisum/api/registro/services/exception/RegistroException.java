package cl.nisum.api.registro.services.exception;

public class RegistroException extends RuntimeException {
    public RegistroException(Long id) {
        super("Registro usuario id not found : " + id);
    }

    public RegistroException(String mensaje) {
        super(mensaje);
    }
}
