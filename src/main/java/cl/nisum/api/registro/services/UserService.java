package cl.nisum.api.registro.services;

import cl.nisum.api.registro.models.User;
import cl.nisum.api.registro.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<User> list() {
        return userRepository.findAll();
    }

    public User create(User user) {
        user.setCreated(new Date());
        user.setLastLogin(new Date());

        return userRepository.save(user);
    }

    public User get(Long id) {
        return userRepository.getOne(id);
    }
}
