package cl.nisum.api.registro.models;

import javax.persistence.*;

/**
 *
 */
@Entity(name = "ForeignKeyAssoPhoneEntity")
@Table(name = "PHONE", uniqueConstraints = {
        @UniqueConstraint(columnNames = "ID")})
public class Phone {
    @Id
    @GeneratedValue
    private Long id;
    private String number;
    private String cityCode;
    private String contryCode;
    @ManyToOne
    private User user;

    public Phone() {
        super();
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getContryCode() {
        return contryCode;
    }

    public void setContryCode(String contryCode) {
        this.contryCode = contryCode;
    }

    public Long getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
